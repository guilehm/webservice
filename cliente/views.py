from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import ClienteSerializer
from .models import Cliente

# Create your views here.
def index(request):
    """A página inicial de Clientes"""
    return render(request, 'cliente/index.html')

def clientes(request):
    clts = Cliente.objects.all()
    context = {
        'clientes' : clts,
    }
    lista = []
    for i in clts:
        cliente = {'nome' : i.nome + ' ' + i.sobrenome,'cpf' : i.cpf, 'idade' : i.idade, 'sexo' : i.sexo}
        lista.append(cliente)
    print(lista)

    return render(request, 'cliente/clientes.html', context)

def cliente(request, cliente_id):
    cliente = Cliente.objects.get(id=cliente_id)
    context = {
        'cliente' : cliente,
    }
    return render(request, 'cliente/cliente.html', context)



class ClienteLista(APIView):

    def get(self, request):
        clientes = Cliente.objects.all()
        serializer = ClienteSerializer(clientes, many=True)
        return Response(serializer.data)

    def post(self,request):
        serializer = ClienteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ClienteFiltro(APIView):

    def post(self, request):
        pesquisa = request.data
        termo = pesquisa['pesquisa']
        clientes = Cliente.objects.all().filter(
            Q(nome__icontains=termo) |
            Q(sobrenome__icontains=termo) |
            Q(cpf__icontains=termo) |
            Q(idade__icontains=termo)
        )

        serializer = ClienteSerializer(clientes, many=True)
        return Response(serializer.data)


class ClienteDetalhe(APIView):

    def get(self, request, cliente_id):
        cliente = Cliente.objects.get(pk=cliente_id)
        serializer = ClienteSerializer(cliente)
        return Response(serializer.data)

    def post(self,request, cliente_id):
        cliente = Cliente.objects.get(pk=cliente_id)
        serializer = ClienteSerializer(data=request.data)
        if serializer.is_valid():
            # serializer.save()
            r = request.POST
            cliente.nome = r['nome']
            cliente.sobrenome = r['sobrenome']
            cliente.cpf = r['cpf']
            cliente.idade = r['idade']
            cliente.sexo = r['sexo']
            cliente.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)

    def delete (self,request, cliente_id):
        serializer = Cliente.objects.get(pk=cliente_id)
        serializer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)