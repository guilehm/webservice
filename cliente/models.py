from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class Cliente(models.Model):
    nome = models.CharField(max_length=100)
    sobrenome = models.CharField(max_length=100)
    cpf = models.CharField(max_length=14)
    opcoes = (('0', 'Masculino'),('1', 'Feminino'))
    sexo = models.CharField(max_length=10, choices=opcoes)
    idade = models.IntegerField(
        validators=[MaxValueValidator(130),
                    MinValueValidator(0)
                ]
        )
    data_adc = models.DateField(auto_now_add=True)
    data_upd = models.DateField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return (self.nome + " " + self.sobrenome)