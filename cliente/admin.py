from django.contrib import admin
from cliente.models import Cliente

# Register your models here.
class ClienteAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'sexo', 'data_adc', 'data_upd']
    class Meta:
        model = Cliente


admin.site.register(Cliente, ClienteAdmin)