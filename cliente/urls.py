from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    # Página inicial
    url(r'^$', views.index, name='index'),
    url(r'^clientes/$', views.clientes, name = 'clientes'),
    url(r'clientes/(?P<cliente_id>\d+)/$', views.cliente, name='cliente'),
    url(r'clientes-api/$', views.ClienteLista.as_view(), name='clientes-api'),
    url(r'clientes-api/(?P<cliente_id>\d+)/$', views.ClienteDetalhe.as_view(), name='cliente-api'),
    url(r'clientes-api/pesquisar/$', views.ClienteFiltro.as_view(), name='cliente-filtro'),
]

urlpatterns = format_suffix_patterns(urlpatterns)