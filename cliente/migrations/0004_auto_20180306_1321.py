# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0003_auto_20180305_2031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cliente',
            name='data_adc',
            field=models.DateField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='data_upd',
            field=models.DateField(auto_now=True),
        ),
    ]
