# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('nome', models.CharField(max_length=100)),
                ('sobrenome', models.CharField(max_length=100)),
                ('sexo', models.CharField(max_length=10, choices=[('0', 'Masculino'), ('1', 'Feminino')])),
                ('data_nasc', models.DateTimeField()),
                ('data_adc', models.DateTimeField(auto_now_add=True)),
                ('data_upd', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
