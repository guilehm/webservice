# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('cliente', '0002_auto_20180305_2027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cliente',
            name='idade',
            field=models.IntegerField(validators=[django.core.validators.MaxValueValidator(130), django.core.validators.MinValueValidator(0)]),
        ),
    ]
