# REST API

**An REST API built in Django.**

## Features

* Bootstrap-themed pages
* Interface to view the clients data and access the API to return the JSON files
* The API is hosted on Heroku through this link: http://gui-webservice.herokuapp.com/
* This API allows you to Create, Delete, Edit and Search informations on database.
* You can do that requesting via GET or POST through this link: http://gui-request.herokuapp.com/

## General information:

* At this link: http://gui-webservice.herokuapp.com/clientes/ you can see the Clients stored on database
* At this link: http://gui-webservice.herokuapp.com/clientes-api/ you can see the JSON data
* At this link: http://gui-webservice.herokuapp.com/clientes-api/20/ you can see the JSON data of specific Client through his ID

## Built With:

* Python 3.6.4
* Django Framework 1.8.4

## Contributing

If you want to contribute, just open an issue and tell me where I can improve.
Fork the repository and change whatever you'd like.
You can send Pull requests.

--------------------------------------------------------------------------------------------
